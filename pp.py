#!/usr/bin/env python2

from glob import iglob
from subprocess import Popen, PIPE, call
import argparse
import logging
import math
import os
import re
import select
import stat
import sys
import time

PROGRESS_RE = re.compile(r'^(?P<bytes>\d+) bytes \((?P<copied>[^\)]+)\) copied, (?P<time>[^,]+ s), (?P<speed>.*?)$', re.M)

BYTES_TO_READABLE = (
    (1024 ** 4, 'TB'),
    (1024 ** 3, 'GB'),
    (1024 ** 2, 'MB'),
    (1024, 'KB'),
    (0, 'B'),
)

VERBOSITY = {
    0: logging.CRITICAL,
    1: logging.ERROR,
    2: logging.WARN,
    3: logging.INFO,
    4: logging.DEBUG
}

logging.basicConfig(format='%(message)s', stream=sys.stderr)
log = logging.getLogger('pp')


class DDWrapper(object):

    def __init__(self, verbosity=None):
        try:
            self.log_level = VERBOSITY[verbosity]
        except KeyError:
            self.log_level = logging.WARN
        log.setLevel(self.log_level)

        self.pipe = None
        self.multifile = False
        self.of_base = None
        self.total_time = 0
        self.total_bytes = 0

    def __call__(self, oflag=None, **kwargs):
        if oflag is None:
            log.debug('Inserting oflag=direct,sync as an argument for more accurate progress')
            kwargs['oflag'] = 'direct,sync'

        # get some information about the source file(s)
        try:
            input_file = kwargs['if']
        except KeyError:
            log.error('Invalid input file')
            sys.exit(1)

        all_files = list(iglob(input_file))
        self.multifile = len(all_files) > 1
        if self.multifile:
            self.of_base = os.path.dirname(kwargs.get('of', '.'))

        for filename in all_files:
            self.copy_file(filename, **kwargs)

        total_copied = self.total_bytes
        for min_value, label in BYTES_TO_READABLE:
            if total_copied >= min_value:
                value = float(total_copied) / min_value
                total_copied = '%.02f %s' % (value, label)
                break

        log.info('Copied %s in %i seconds' % (total_copied, self.total_time))

    def copy_file(self, filename, **kwargs):
        """Copy an individual file."""

        start = time.time()
        try:
            log.debug('Attempting to stat: %s' % (filename,))
            info = os.stat(filename)
        except OSError as exc:
            log.error(str(exc))
            sys.exit(2)

        max_size = info.st_size or 1

        # see if we're dealing with a block device
        if stat.S_ISBLK(info.st_mode):
            log.debug('We are dealing with a block device')
            fd = os.open(filename, os.O_RDONLY)
            try:
                max_size = os.lseek(fd, 0, os.SEEK_END)
            finally:
                os.close(fd)

        name = os.path.basename(filename)
        log.debug('Detected filesize: %s' % (max_size,))
        if self.multifile:
            kwargs['if'] = filename
            kwargs['of'] = os.path.join(self.of_base, name)

        args = ['%s=%s' % pair for pair in kwargs.items()]
        self.pipe = Popen(['dd'] + args, stdout=PIPE, stderr=PIPE)
        self.get_status()

        log.warn(name)
        while self.pipe.poll() is None:
            self.show_progress(max_size)

        self.show_progress(max_size, True)
        sys.stdout.write('\n')

        duration = time.time() - start
        self.total_time += duration
        self.total_bytes += max_size
        log.info('%s\tDuration: %i second(s)' % (name, duration))

    def get_status(self):
        call(['kill', '-SIGUSR1', str(self.pipe.pid)], stderr=PIPE)

    def show_progress(self, max_size, full=False):
        r, w, x = select.select([self.pipe.stdout, self.pipe.stderr], [], [], 1)
        for source in r:
            if full:
                line = source.read()
            else:
                line = source.readline()

            m = PROGRESS_RE.search(line)
            if not m:
                continue

            log.debug('Matched progress line: %s' % (m.groupdict(),))
            copied = int(m.group('bytes'))
            progress = copied / float(max_size) * 100.0
            p = min(progress, 100)

            # display the progress bar
            if self.log_level > logging.DEBUG:
                sys.stdout.write('\r[%s] %s%%, %s copied at %s     ' % (
                    ('#' * int(math.ceil(p / 2))).ljust(50, ' '), int(p),
                    m.group('copied'), m.group('speed')
                ))
                sys.stdout.flush()
            else:
                log.debug('%s%%, %s copied at %s' % (
                    int(p), m.group('copied'), m.group('speed')
                ))

            log.debug('Asking for progress')
            self.get_status()
            time.sleep(0.25)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Wrapper around dd to show progress')
    parser.add_argument('-v', '--verbosity', choices=range(5), type=int,
        default=2, help='Verbosity level')

    opts, args = parser.parse_known_args()

    try:
        kwargs = dict(arg.split('=', 1) for arg in args)

        ddw = DDWrapper(**opts.__dict__)
        ddw(**kwargs)
    except (KeyboardInterrupt, SystemExit):
        pass
    except:
        log.exception('An error has occurred')
